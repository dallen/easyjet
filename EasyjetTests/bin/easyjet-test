#!/usr/bin/env bash

set -Eeu


################################################
# mode-based switches
################################################

#tag from easyjet/hh4b-test-files
TAG=rel25019

# standard samples
# Data
PHYS_DATA_2018=data18_13TeV.00362204.physics.DAOD_PHYS_10evts.r13286_p4910_p6479.pool.root
PHYSLITE_DATA_2018=data18_13TeV.00362204.physics.DAOD_PHYSLITE_10evts.r13286_p4910_p6479.pool.root
PHYS_DATA_2022=data22_13p6TeV.00436656.physics_Main.DAOD_PHYS_10evts.r14755_p5604_p6482.pool.root
PHYSLITE_DATA_2022=data22_13p6TeV.00436656.physics_Main.DAOD_PHYSLITE_10evts.r14755_p5604_p6482.pool.root

# MC20
PHYS_TTBAR_MC20=mc20_13TeV.410470.PhPy8EG_A14_ttbar_hdamp258p75_nonallhad.DAOD_PHYS_10evts.e6337_s3681_r13144_r13146_p6490.pool.root
PHYSLITE_TTBAR_MC20=mc20_13TeV.410470.PhPy8EG_A14_ttbar_hdamp258p75_nonallhad.DAOD_PHYSLITE_10evts.e6337_s3681_r13144_r13146_p6490.pool.root
PHYS_BBYY_MC20=mc20_13TeV.600021.PhPy8EG_PDF4LHC15_HHbbyy_cHHH01d0.DAOD_PHYS_10evts.e8222_s3681_r13167_r13146_p6490.pool.root
PHYSLITE_BBYY_MC20=mc20_13TeV.600021.PhPy8EG_PDF4LHC15_HHbbyy_cHHH01d0.DAOD_PHYSLITE_10evts.e8222_s3681_r13167_r13146_p6490.pool.root
PHYS_SH4B_BOOSTED_MC20_AF3=mc20_13TeV.801636.Py8EG_A14NNPDF23LO_XHS_X3000_S70_4b.DAOD_PHYS_10evts.e8448_a907_r14860_p6490.pool.root
PHYSLITE_SH4B_BOOSTED_MC20_AF3=mc20_13TeV.801636.Py8EG_A14NNPDF23LO_XHS_X3000_S70_4b.DAOD_PHYSLITE_10evts.e8448_a907_r14860_p6490.pool.root

# MC23
PHYS_TTBAR_MC23=mc23_13p6TeV.601229.PhPy8EG_A14_ttbar_hdamp258p75_SingleLep.DAOD_PHYS_10evts.e8514_s4159_r15530_p6491.pool.root
PHYSLITE_TTBAR_MC23=mc23_13p6TeV.601229.PhPy8EG_A14_ttbar_hdamp258p75_SingleLep.DAOD_PHYSLITE_10evts.e8514_s4159_r15530_p6491.pool.root
PHYS_JETS_MC23_AF3=mc23_13p6TeV.801171.Py8EG_A14NNPDF23LO_jj_JZ6.DAOD_PHYS_10evts.e8514_e8528_a911_s4114_r15224_r15225_p6491.pool.root
PHYSLITE_JETS_MC23_AF3=mc23_13p6TeV.801171.Py8EG_A14NNPDF23LO_jj_JZ6.DAOD_PHYSLITE_10evts.e8514_e8528_a911_s4114_r15224_r15225_p6491.pool.root

# Test name convention
# [CONFIG_INPUTTYPE]


declare -A DATAFILES=(
    # Data
    [fast-data18]=${PHYS_DATA_2018}
    [fast-data22]=${PHYS_DATA_2022}
    [bbtt-data22]=${PHYS_DATA_2022}
    [bbtt-data22-lite]=${PHYSLITE_DATA_2022}
    [ttHH-data22-lite]=${PHYSLITE_DATA_2022}
    [bbyy-data22-lite]=${PHYSLITE_DATA_2022}
    [SHbbyy-data22-lite]=${PHYSLITE_DATA_2022}
    # MC20
    [mc20]=${PHYS_TTBAR_MC20}
    [fast-mc20]=${PHYS_TTBAR_MC20}
    [bbyy-mc20-syst]=${PHYS_BBYY_MC20}
    [bbyy-mc20-lite]=${PHYSLITE_BBYY_MC20}
    [SHbbyy-mc20]=${PHYS_BBYY_MC20}
    [ttHH-mc20]=${PHYS_TTBAR_MC20}
    [ttHH-mc20-lite]=${PHYSLITE_TTBAR_MC20}
    [bbll-mc20]=${PHYS_TTBAR_MC20}
    [bbll-mc20-lite-syst]=${PHYSLITE_TTBAR_MC20}
    [lltt-mc20]=${PHYS_TTBAR_MC20}
    [lltt-mc20-lite]=${PHYSLITE_TTBAR_MC20}
    [vbshiggs-mc20]=${PHYS_TTBAR_MC20}
    [vbshiggs-mc20-lite]=${PHYSLITE_TTBAR_MC20}
    [VBSVV4q-mc20]=${PHYS_TTBAR_MC20}
    [VBSVV4q-mc20-lite]=${PHYSLITE_TTBAR_MC20}
    [ssWW-mc20]=${PHYS_TTBAR_MC20}
    [ssWW-mc20-lite]=${PHYSLITE_TTBAR_MC20}
    [ZCharm-mc20]=${PHYS_TTBAR_MC20}
    [ZCharm-mc20-lite]=${PHYSLITE_TTBAR_MC20}
    [llyy-mc20]=${PHYS_TTBAR_MC20}
    # MC20 AF3
    [mc20af3-lite]=${PHYSLITE_SH4B_BOOSTED_MC20_AF3}
    [fast-mc20af3]=${PHYS_SH4B_BOOSTED_MC20_AF3}
    [fast-mc20af3-lite]=${PHYSLITE_SH4B_BOOSTED_MC20_AF3}
    [sh4b-mc20af3]=${PHYS_SH4B_BOOSTED_MC20_AF3}
    [fast-awkward-mc20af3]=${PHYS_SH4B_BOOSTED_MC20_AF3}
    [SHbbyy-mc20af3-lite]=${PHYSLITE_SH4B_BOOSTED_MC20_AF3}
    # MC23
    [mc23]=${PHYS_TTBAR_MC23}
    [fast-mc23]=${PHYS_TTBAR_MC23}
    [mc23af3]=${PHYS_JETS_MC23_AF3}
    [SHbbyy-mc23af3]=${PHYS_JETS_MC23_AF3}
    [bbbb-mc23]=${PHYS_TTBAR_MC23}
    [bbtt-mc23]=${PHYS_TTBAR_MC23}
    [bbVV-mc23]=${PHYS_TTBAR_MC23}
    [bbtt-mc23-syst]=${PHYS_TTBAR_MC23}
    [bbtt-mc23-lite-syst]=${PHYSLITE_TTBAR_MC23}
    [bbll-mc23-lite]=${PHYSLITE_TTBAR_MC23}
    [bbyy-mc23-lite]=${PHYSLITE_TTBAR_MC23}
    [emtopo-mc23]=${PHYS_TTBAR_MC23}
    [hhml-mc23]=${PHYS_TTBAR_MC23}
    [hhml-mc23-lite]=${PHYSLITE_TTBAR_MC23}
    [llyy-mc23]=${PHYS_TTBAR_MC23}
    # systematics enabled
    [syst-mc23]=${PHYS_TTBAR_MC23}
    # non-ntupler workflows
    [aod-fast-bbbb-mc23]=${PHYS_TTBAR_MC23}
    [h5-fast-mc23]=${PHYS_TTBAR_MC23}
    [hist-syst-bbbb-mc23]=${PHYS_TTBAR_MC23}
    # test mode
    [fail]=${PHYS_TTBAR_MC23}
)

declare -A TESTS=(
    # Data
    [fast-data18]="data-fast easyjet-ntupler"
    [fast-data22]="data-fast easyjet-ntupler"
    [bbtt-data22]="simple bbtt-ntupler"
    [bbtt-data22-lite]="simple bbtt-ntupler"
    [ttHH-data22-lite]="simple ttHH-ntupler"
    [bbyy-data22-lite]="simple bbyy-ntupler"
    [SHbbyy-data22-lite]="simple bbyy-ntupler"
    # MC20
    [mc20]="simple easyjet-ntupler"
    [fast-mc20]="fast easyjet-ntupler"
    [bbyy-mc20-syst]="simple bbyy-ntupler"
    [bbyy-mc20-lite]="simple bbyy-ntupler"
    [SHbbyy-mc20]="simple bbyy-ntupler"
    [ttHH-mc20]="simple ttHH-ntupler"
    [ttHH-mc20-lite]="simple ttHH-ntupler"
    [bbll-mc20]="simple bbll-ntupler"
    [bbll-mc20-lite-syst]="simple bbll-ntupler"
    [lltt-mc20]="simple lltt-ntupler"
    [lltt-mc20-lite]="simple lltt-ntupler"
    [vbshiggs-mc20]="simple vbshiggs-ntupler"
    [vbshiggs-mc20-lite]="simple vbshiggs-ntupler"
    [VBSVV4q-mc20]="simple VBSVV4q-ntupler"
    [VBSVV4q-mc20-lite]="simple VBSVV4q-ntupler"
    [ssWW-mc20]="simple ssWW-ntupler"
    [ssWW-mc20-lite]="simple ssWW-ntupler"
    [ZCharm-mc20]="simple ZCharm-ntupler"
    [ZCharm-mc20-lite]="simple ZCharm-ntupler"
    [llyy-mc20]="simple llyy-ntupler" 
    # MC20 AF3
    [mc20af3-lite]="simple easyjet-ntupler"
    [fast-mc20af3]="fast easyjet-ntupler"
    [fast-mc20af3-lite]="fast easyjet-ntupler"
    [sh4b-mc20af3]="simple bbbb-ntupler"
    [fast-awkward-mc20af3]="fast easyjet-ntupler"
    [SHbbyy-mc20af3-lite]="simple bbyy-ntupler"
    # MC23
    [fast-mc23]="fast easyjet-ntupler"
    [mc23]="simple easyjet-ntupler"
    [mc23af3]="simple easyjet-ntupler"
    [SHbbyy-mc23af3]="simple bbyy-ntupler"
    [bbbb-mc23]="simple bbbb-ntupler"
    [bbtt-mc23]="simple bbtt-ntupler"
    [bbVV-mc23]="simple bbVV-ntupler"
    [bbtt-mc23-syst]="simple bbtt-ntupler"
    [bbtt-mc23-lite-syst]="simple bbtt-ntupler"
    [bbll-mc23-lite]="simple bbll-ntupler"
    [bbyy-mc23-lite]="simple bbyy-ntupler"
    [syst-mc23]="systematics easyjet-ntupler"
    [emtopo-mc23]="simple easyjet-ntupler"
    [hhml-mc23]="simple hhml-ntupler"
    [hhml-mc23-lite]="simple hhml-ntupler"
    [llyy-mc23]="simple llyy-ntupler" 
    # non-ntupler
    [aod-fast-bbbb-mc23]="aod-fast bbbb-ntupler"
    [h5-fast-mc23]="h5-fast easyjet-ntupler"
    [hist-syst-bbbb-mc23]="hist-syst bbbb-ntupler"
    # test mode
    [fail]=false
)

DEFAULT_CONFIG=EasyjetHub/RunConfig.yaml
declare -A CONFIGS=(
    # Data
    [bbtt-data22]=bbttAnalysis/RunConfig-bbtt-bypass.yaml
    [bbtt-data22-lite]=bbttAnalysis/RunConfig-bbtt-bypass.yaml
    [ttHH-data22-lite]=ttHHAnalysis/RunConfig-ttHH-bypass.yaml
    [bbyy-data22-lite]=bbyyAnalysis/RunConfig-bbyy-skimming-legacy-bypass.yaml
    [SHbbyy-data22-lite]=bbyyAnalysis/RunConfig-Resonant-bypass.yaml
    # MC20
    [bbyy-mc20-syst]=bbyyAnalysis/RunConfig-bbyy-bypass-syst.yaml
    [bbyy-mc20-lite]=bbyyAnalysis/RunConfig-bbyy-skimming-legacy-bypass.yaml
    [SHbbyy-mc20]=bbyyAnalysis/RunConfig-Resonant-bypass.yaml
    [ttHH-mc20]=ttHHAnalysis/RunConfig-ttHH-bypass.yaml
    [ttHH-mc20-lite]=ttHHAnalysis/RunConfig-ttHH-bypass.yaml
    [bbll-mc20]=bbllAnalysis/RunConfig-bbll-bypass.yaml
    [bbll-mc20-lite-syst]=bbllAnalysis/RunConfig-bbll-Resonant-NW-syst-bypass.yaml
    [lltt-mc20]=llttAnalysis/RunConfig-lltt-bypass.yaml
    [lltt-mc20-lite]=llttAnalysis/RunConfig-lltt-bypass.yaml
    [vbshiggs-mc20]=vbshiggsAnalysis/RunConfig-fullLep-bypass.yaml
    [vbshiggs-mc20-lite]=vbshiggsAnalysis/RunConfig-fullLep-bypass.yaml
    [VBSVV4q-mc20]=VBSVV4qAnalysis/RunConfig-JJ-bypass.yaml
    [VBSVV4q-mc20-lite]=VBSVV4qAnalysis/RunConfig-JJ-bypass.yaml
    [ssWW-mc20]=ssWWAnalysis/RunConfig-Signal-ssWW-bypass.yaml
    [ssWW-mc20-lite]=ssWWAnalysis/RunConfig-Signal-ssWW-bypass.yaml
    [ZCharm-mc20]=ZCharmAnalysis/RunConfig-ZCharm-bypass.yaml
    [ZCharm-mc20-lite]=ZCharmAnalysis/RunConfig-ZCharm-bypass.yaml
    [llyy-mc20]=HZallyyAnalysis/RunConfig-llyy-bypass.yaml
    # MC20 AF3
    [fast-mc20af3]=EasyjetHub/RunConfig-noEle.yaml
    [fast-mc20af3-lite]=EasyjetHub/RunConfig-PHYSLITE-noEle.yaml
    [mc20af3-lite]=EasyjetHub/RunConfig-PHYSLITE-noEle.yaml
    [sh4b-mc20af3]=bbbbAnalysis/RunConfig-SH4b.yaml
    [fast-awkward-mc20af3]=EasyjetHub/awkward-config-noEle.yaml
    [SHbbyy-mc20af3-lite]=bbyyAnalysis/RunConfig-Resonant-bypass.yaml
    # MC23
    [bbbb-mc23]=bbbbAnalysis/RunConfig-HH4b-All-NoSyst-bypass.yaml
    [bbtt-mc23]=bbttAnalysis/RunConfig-bbtt-bypass.yaml
    [bbVV-mc23]=bbVVAnalysis/RunConfig-bbVV-bypass.yaml
    [bbtt-mc23-syst]=bbttAnalysis/RunConfig-bbtt-bypass-syst.yaml
    [bbtt-mc23-lite-syst]=bbttAnalysis/RunConfig-bbtt-bypass-syst.yaml
    [bbll-mc23-lite]=bbllAnalysis/RunConfig-bbll-bypass.yaml
    [bbyy-mc23-lite]=bbyyAnalysis/RunConfig-bbyy-skimming-legacy-bypass.yaml
    [emtopo-mc23]=EasyjetHub/RunConfig-EMTopo.yaml
    [hhml-mc23]=multileptonAnalysis/RunConfig-multilepton-bypass.yaml
    [hhml-mc23-lite]=multileptonAnalysis/RunConfig-multilepton-bypass.yaml
    [llyy-mc23]=HZallyyAnalysis/RunConfig-llyy-bypass.yaml
    # MC23 AF3
    [mc23af3]=EasyjetHub/RunConfig-noEle.yaml
    [SHbbyy-mc23af3]=bbyyAnalysis/RunConfig-Resonant-bypass.yaml
    # non-ntupler
    [aod-fast-bbbb-mc23]=bbbbAnalysis/RunConfig-HH4b-Resolved.yaml
    [hist-syst-bbbb-mc23]=bbbbAnalysis/histonly-config.yaml
)

# some common variables in the tests
COMMON="-O"

# specific tests
simple() {
    local CMD="$1 $2 -c $3 $COMMON -o analysis.root"
    echo "running $CMD"
    $CMD
    metadata-check -v
}
systematics() {
    local CMD="$1 $2 -c $3 -o $COMMON --do-CP-systematics True  --systematics-regex .*"
    echo "running $CMD"
    $CMD
    metadata-check -v
}
fast() {
    local OPTS="$COMMON --fast-test --cache-metadata"
    OPTS+=" -o analysis.root --h5-output test.h5"
    local CMD="$1 $2 -c $3 $OPTS"
    $CMD --stop-after-config
    CMD+=" --timeout 50 "
    echo "running $CMD"
    $CMD
    metadata-check -v
}
data-fast() {
    local OPTS="$COMMON --fast-test --cache-metadata"
    OPTS+=" -o analysis.root --h5-output test.h5"
    OPTS+=" --do-trigger-filtering False"
    local CMD="$1 $2 -c $3 $OPTS"
    $CMD --stop-after-config
    CMD+=" --timeout 50 "
    echo "running $CMD"
    $CMD
    metadata-check -v
}
h5-fast() {
    local OPTS="$COMMON --fast-test --cache-metadata"
    OPTS+=" --h5-output test.h5"
    local CMD="$1 $2 -c $3 $OPTS"
    $CMD --stop-after-config
    CMD+=" --timeout 50 "
    echo "running $CMD"
    $CMD
    metadata-check -v
}
aod-fast() {
    local AOD=test.pool.root
    local OPTS="$COMMON --fast-test --cache-metadata"
    OPTS+=" --output-xaod ${AOD}"
    local CMD="$1 $2 -c $3 $OPTS"
    $CMD --stop-after-config
    CMD+=" --timeout 50 "
    echo "running $CMD"
    $CMD
    metadata-check -v
    local HISTS=aod-hists.h5
    CMD="bbbb-hists ${AOD} -o $HISTS"
    echo "running $CMD"
    $CMD
    echo "made hists:"
    h5ls ${HISTS}/nominal
}
hist-syst() {
    local OPTS="$COMMON --fast-test --cache-metadata"
    OPTS+=" --output-hists hists.h5"
    local CMD="$1 $2 -c $3 $OPTS"
    $CMD --stop-after-config
    CMD+="  --timeout 50"
    echo "running $CMD"
    $CMD
    metadata-check -v
}

################################################
# parse arguments
################################################

ALL_MODES=${!TESTS[*]}
DIRECTORY=${EASYJET_TESTDIR-""}
DATA_URL=https://gitlab.cern.ch/easyjet/hh4b-test-files/-/raw
LOG_LEVEL=${EASYJET_LOG_LEVEL-"WARNING"}
NPROCS=${EASYJET_NPROCS-$(nproc)}
# metatest means we stuff in an echo prefix to commands
if [[ ${EASYJET_METATEST+x} ]]; then
    METASTATUS=set
    PFX=echo
else
    METASTATUS=unset
    PFX=""
fi

print-usage() {
    echo "usage: ${0##*/} [-h] [-l <level>] [-L <log-file>] [-d <dir>] <mode>" 1>&2
}

usage() {
    print-usage
    exit 1;
}

help() {
    print-usage
    cat <<EOF

The ${0##*/} utility will download a test DAOD file and run it.

Options:
 -d <dir>: specify directory to run in
 -l <level>: set log level
 -L <log-file>: specify log file into which to direct output
 -h: print help

Run modes:
$(dump-modes)
  all -> run all of the above (for local testing)
  all-exit-early -> all, but exit immediately if any test fails

Notes on environment variables:
 - EASYJET_LOG_LEVEL: default for -l, currently ${LOG_LEVEL}
 - EASYJET_TESTDIR: default for -d, currently ${DIRECTORY:-/tmp/<random>}
 - EASYJET_NPROCS: number of processes with "all", currently ${NPROCS}
 - EASYJET_METATEST: set to run a test test, currently ${METASTATUS}

EOF
    exit 1
}

# Terminal magic: if stdout is set to go to a terminal we'll add
# colors to the help output below. Otherwise we strip this stuff
# out. Note that you have to check the interactivity here: within the
# function the output stream is never seen as going to the terminal.
if [[ -t 1 ]]; then
    INTERACTIVE=yes
else
    INTERACTIVE=""
fi
dump-modes() {
    if [[ ${INTERACTIVE} ]]; then
        local RB=$(tput setaf 1)$(tput bold)
        local NO=$(tput sgr0)
        local Y=$(tput setaf 3)
        local G=$(tput setaf 2)
        local FMT="  %s ${Y}-> ${RB}%s${NO} ${G}%s${NO} %s\n"
    else
        local FMT="  %s -> %s %s %s\n"
    fi
    local mode
    for mode in ${ALL_MODES[*]/fail}
    do
        local func=${TESTS[$mode]}
        local config=${CONFIGS[$mode]-$DEFAULT_CONFIG}
        local data=${DATAFILES[$mode]}
        printf "${FMT}" $mode "$func" $config $data
    done
}

OPT_KEYS=":d:hl:L:"
dump-complete() {
    printf "%s " ${ALL_MODES[*]} all all-exit-early
    local char
    echo -n ${OPT_KEYS//:} | while read -n 1 char
    do
        printf -- "-%s " $char
    done
}

# the c option is "hidden", we just use it to pass options to tab
# complete
while getopts ${OPT_KEYS}c o; do
    case "${o}" in
        d) DIRECTORY=${OPTARG} ;;
        h) help ;;
        l) LOG_LEVEL=${OPTARG} ;;
        L) LOG_FILE=${OPTARG} ;;
        # this is just here for tab complete
        c) dump-complete; exit 0 ;;
        *) usage ;;
    esac
done
shift $((OPTIND-1))

if (( $# != 1 )) ; then
    usage
fi

MODE=$1
if [[ $MODE == "all-exit-early" ]]; then
    MODE="all"
    EXIT_EARLY=1
fi

# add common options
COMMON+=" -l ${LOG_LEVEL}"

############################################
# Check that all the modes / paths exist
############################################
#

verify-test() {
    if [[ ! ${DATAFILES[$1]+x} ]]; then usage; fi
    if [[ ! ${TESTS[$1]+x} ]]; then usage; fi
}

if [[ ${MODE} == "all" ]]; then
    for mode in $ALL_MODES
    do
        verify-test $mode
    done
else
    verify-test $MODE
fi

#############################################
# now start doing stuff
#############################################
#
if [[ -z ${DIRECTORY} ]] ; then
    DIRECTORY=$(mktemp -d)
    echo "running in ${DIRECTORY}" >&2
fi

if [[ ! -d ${DIRECTORY} ]]; then
    if [[ -e ${DIRECTORY} ]] ; then
        echo "${DIRECTORY} is not a directory" >&2
        exit 1
    fi
    mkdir ${DIRECTORY}
fi
cd $DIRECTORY

error-handler() {
    echo "$1 failed with exit code $?" >> test_results.log
}

run-test() {
    local mode=$1
    local download_path=${DATAFILES[$mode]}
    local file=${download_path##*/}
    local config=${CONFIGS[$mode]-$DEFAULT_CONFIG}

    # get files
    if [[ ! -f ${file} ]] ; then
        echo "getting file ${file} from ${DATA_URL}"
        ${PFX} curl -s ${DATA_URL}/${TAG}/${download_path} > ${file}
    fi

    local testcmd="${PFX} ${TESTS[$mode]} $file $config"
    trap "error-handler $mode" ERR
    if [[ ! -z ${LOG_FILE+x} ]]; then
	echo "calling: $testcmd > $LOG_FILE 2>&1"
        $testcmd > $LOG_FILE 2>&1
    else
	echo "calling: $testcmd"
        $testcmd
    fi
    echo "$mode succeeded" >> test_results.log
}

copy-ntuple(){
    local mode=$1
    local postfix="${mode//-/_}"
    
    if [[ -z ${CI_JOB_NAME+x} || ! $CI_JOB_NAME == *"ntupler_"* ]]; then
	echo "Not an ntupler job, will not copy root output to eos"
	return 0
    fi
    if [[ -z ${EOS_ACCOUNT_PASSWORD+x} || -z ${EOS_ACCOUNT_USERNAME+x} || -z ${EOS_ACCOUNT_PATH+x} ]]; then 
	echo "eos env not defined, will not copy root output to eos"
	return 0
    fi
    if [[ -z ${CI_PROJECT_PATH+x} || ! $CI_PROJECT_PATH == "easyjet/easyjet" ]]; then 
	echo "not running on easyjet main repo CI, will not copy root output to eos"
	return 0
    fi
    if [[ -z "${CI_MERGE_REQUEST_IID:-}" ]]; then
       echo "merge request ID not defined, will not copy root output to eos"
       return 0
    fi

    local eosdir=$EOS_ACCOUNT_PATH/gitlab_ci_output/ntuples/
    local eostmpdir=$CI_MERGE_REQUEST_IID
    echo "copying ntuple to $eosdir"
    echo $EOS_ACCOUNT_PASSWORD | kinit -l 1h $EOS_ACCOUNT_USERNAME 
    klist
    mkdir -p $eostmpdir
    cp analysis.root $eostmpdir/analysis_${postfix}.root
    echo $CI_MERGE_REQUEST_TITLE > $eostmpdir/CI_MERGE_REQUEST_TITLE
    ${PFX} xrdcp -rf $eostmpdir  root://eosuser.cern.ch//$eosdir
    echo "copying ntuple succeeded"
}

test-summary() {
    if [[ ! -z ${EXIT_EARLY+x} ]] ; then
        echo "Waiting 10s to collect test results from other incomplete jobs"
        sleep 10
    fi
    printf "\nAvailable test results:\n\n"
    for result in */test_results.log; do
        cat $result >> test_results.log
    done
    cat test_results.log
}

# now run the test
if [[ ${MODE} == "all" ]] ; then
    # Run in parallel -- each job is in a separate shell
    testcmd="${0} -d {} -L test.log {}"
    if [[ ! -z ${EXIT_EARLY+x} ]] ; then
        testcmd+=" || exit 255"
    fi
    PMODES=${ALL_MODES[*]/fail}
    trap "echo failed tests detected; test-summary" ERR
    printf "%s\n" $PMODES | xargs -P $NPROCS -I {} bash -c "${testcmd}" &> /dev/null

    printf "\nAll tests completed successfully\n"
else
    echo "running ${MODE}"
    trap "echo test failed with exit code \$?" ERR
    run-test $MODE
    copy-ntuple $MODE
    echo "${MODE} completed successfully"
fi

echo "=== checking directory content ==="
pwd
ls
echo "=================================="
